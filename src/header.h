/* ***** DESCRIPTION ***** */
/* A stupid shell based on RPG systems, with dice, mana, and some magic things... */


/* ******* LICENSE ******* */
/* Created by "phineas0fog" e.vanespen[at]protonmail.com*/
/* And placed under CC BY-NC-SA license */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

//#include "phineas.c"

// define return codes
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#define RL_BUFSIZE 1024
#define TOK_BUFSIZE 64
#define TOK_DELIM " \t\r\n\a"
